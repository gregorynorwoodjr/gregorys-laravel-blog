## Laravel-PHP Blog

My php laravel web application blog

- Authentication System.
- CRUD functionality for users, posts, tags, and categories.
- Data Validation.
- Soft deleting and retrieval.
- One to many & Many to many relationships.
- Dynamic content display.
